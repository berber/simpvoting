\documentclass{scrartcl}

\usepackage{packages}
\usepackage{commands}

\bibliography{bibliography.bib}

\title{Gibbard-Satterthwaite, Arrow, simplicial complexes, domain restrictions}
\author{Berthold Blatt Lorke}
\date{2023-12-17}

\begin{document}

\sloppy

\maketitle

\section{Setup}

Let $A$ be a finite set, the set of all \emph{alternatives}.

Let $a,b \in A$, and assume we want to somewhere rank $a < b$, while for now not caring about other alternatives.  The literature usually then defines a set $U_{ab}^-$, which consists of all possible orders on $A$ which rank $a < b$.  Similarly one can define $U_{ab}^+ = U_{ba}^-$ which is the set of all possible orders that rank $b>a$.   Then the simplicial complex that becomes relevant is just the nerve of all of these sets.  Importantly, the facets of this simplicial complex, meaning the simplex that corresponds to an intersection of such sets that cannot further be nonemptily intersected with another set, are singletons, so they correspond to a single specific total order.  This way of working with nerves, in general, is a bit peculiar:  A single pair and their relation to each other corresponds to a big set, and a lot of pairs and their relations, which correspond to a high-dimensional simplex, if we have enough, corresponds to a single order.  This construction is also dependent on the set $A$, everything is sort of always \enquote{in context context of $A$}.

Our setup regarding these relevant simplicial complexes for this setting will in some sense be the oposite:  The pair and their relation to each other $a<b$ will correspond to a $0$-simplex, and we form certain unions of these singletons (or subsets of our vertex set), for our higher dimensional simplices, to construct our desired simplicial complex.  And, what we can do here (which we cannot that easily do with the previous construction), is look at it from the other end:  An order corresponds to a facet, and then we can look at the induced simplicial complex, taking all possible subsets.

It should now hopefully be clear what the construction is, and the author believes that this is the most natural construction in the first place:  The pair and their relation to each other $a<b$ corresponds to the singleton $\set{\tuple{a,b}}$, and we allow unions if they don't \enquote{contradict each other}, in other words if there is a possible extension to an order.  The other way around, a specific (strict) order $<$ corresponds to the set of all pairs $\tuple{a,b}$ such that $a<b$, these are the facets, and the rest of the simplices are all subsets of such.  Actually, saying \enquote{an order corresponds to the set of all pairs \dots} is actually too generous:  In most textbooks, this is precisely the definition of an order!  An order is just a specific binary relation on a set, which in turn is just a subset of the set of all tuples $A \cartes A$.  We also have the practical property that if we do everything \enquote{bottom up}, we need not even know what the set of all alternatives is, we can always extend as much as we want and take more unions!

To sum up:  The set $A \times A$ will be the set of all vertices, the facets are orders on $A$, which by definition are subsets of $A \times A$, and we get our desired induced simplicial complex by including all faces of the facets.

\begin{definition}[orders]
  We define the following sets that correspond various kinds of orders.
  \begin{itemize}
  \item $\Wea$ refers to the set of all \emph{weak orders}, or \emph{total preorders}, on $A$, and $\sWea$ refers to the set of all \emph{strict weak orders}.  A weak order is usually denoted by $\worder$, and its corresponding strict order by $\worderstrict$.  If for elements $a,b \in A$ we have $a \worder b$ and $b \worder a$, we write $a \tie b$.
  \item $\Lin$ refers to the set of all \emph{total orders}, and $\sLin$ refers to the set of all \emph{strict weak orders}, which is the same as the set of all \emph{linear orders}, on $A$.  A linear order is usually referred to as $\order$, and its corresponding linear order by $\orderstrict$.
  \item $\pWea$ refers to the set of all \emph{pseudo weak orders} on $A$, which is the set of all relations on $A$ that can be extended to a weak order, and $\psWea$ refers to the set of all \emph{pseudo strict weak orders}, which is the set of all relations on $A$ that can be extended to a strict weak order.
  \item $\pLin$ refers to the set of all \emph{pseudo total orders} on $A$, which is the set of all relations on $A$ that can be extended to a total order, and $\psLin$ refers to the set of all \emph{pseudo strict total orders} or \emph{pseudo linear orders}, which is the set of all relations on $A$ that can be extended to a strict total order.
  \end{itemize}
\end{definition}

\begin{remark}
  \begin{itemize}
  \item $\psLin$ corresponds to the set of all directed acyclic graphs!  We therefore can talk about maximal elements (which corresponds to nodes with no parents).
  \item Maybe the term \enquote{pseudo} is not the best, we should maybe pick another term.
  \item We can define for a pseudo weak order a corresponding pseudo strict weak order (and therefore also for total orders) in the usual way.
  \item With the above comment, the act of taking the strict version of something and the act of taking the pseudo version of something commute.  The abuse of notation $\pseudo{(\strict{T})} = \strict{\pseudo{T}} = \strict{(\pseudo{T})}$ for $T \in \set{\Lin, \Wea}$ is therefore justified.
  \item There are one-to-one correspondences (in the usual way) $T \corresponds \strict{T}$ for $T \in \set{\Lin, \pLin, \Wea, \pWea}$
  \item $\Lin \subseteq \Wea$
  \item $\sLin \not\subseteq \sWea$
  \item $\pLin \subseteq \pWea$
  \item $\psLin \not\subseteq \psWea$
  \item $T \subseteq \pseudo{T}$ for $T \in \set{\Lin, \Wea, \sLin, \sWea}$
  \item $\pWea = \powerset{A}$
  \item We can embed $A \embarrow \powerset{A} \setminus \set{\emptyset} \embarrow \Wea$ and with the usual corresponcences therefore also $A \embarrow \powerset{A} \setminus \set{\emptyset} \embarrow \sWea$.  We can furthermore define the same constructions with pseudo and strict versions.
  % \item With the above correspondence, we have $\pseudo{A} = \powerset{A} \setminus \set{\emptyset}$.
  \item TODO:  While writing, I realize Ishould rewrite all of this.  The correspondence between weak and strict, and a general notion of what taking pseudo versions is, should be general.
  \end{itemize}
\end{remark}

\begin{definition}[linear order simplicial complexes]
  Let $A$ be a finite set.
  We define its corresponding simplicial complex of linear orders as $\mathfrak L \defeq \set{R \subseteq A\times A \given R \in \psLin}$.

  Consider the simplicial complex $\mathfrak C$ defined as the set of all subsets of $A$ of cardinality $2$.  The complex $\mathfrak L$ is a chromatic simplicial complex via the coloring $\chi \mapcolon \mathfrak L \to \mathfrak C$ induced by $\set{\tuple{a,b}} \mapsto \set{\set{a,b}}$, or on vertices, $\tuple{a,b} \mapsto \set{a,b}$.
\end{definition}

\begin{remark}
  \begin{itemize}
  \item This definition indeed gives a simplicial complex:  Let $s \in \mathfrak L$ be a simplex in $\mathfrak L$, meaning that $s$ is a linear order on $A$.  Then there is some total order $t$ such that $s \subseteq t$.  Any subset of $t$ is in $\mathfrak L$, since, since it can be extended to $t$, and therefore via definition is a pseudo linear order.  In particular, any subset of $s$ is in $\mathfrak L$.  Therefore $\mathfrak L$ is a linear order.  The set of vertices is obviously $A \cartes A$.
  \item The facets of $\mathfrak L$ are linear orders, and the simplicial complex is pure of dimension $\binom{\numberof{A}}{2}$.  This is due to the fact that any linear order $f \in L$ precisely contains either $\tuple{a,b}$ or $\tuple{b,a}$ (exclusive or) for all distinct $a,b \in A$, in other words it makes a choice for every unordered pair in $A$, and there are $\binom{\numberof{A}}{2}$ many of those.
  \item $\mathfrak L$ is obviously then the simplicial complex induced by the total orders on $A$, chosen as facets.
  \item $\mathfrak L$ canonically isomorphic to the simplicial complex $N_W$ introduced in \cite{bar93}, via $\set{\tuple{a,b}} \mapsto \mathfrak U_{ab}^-$.
  \end{itemize}
\end{remark}

One of the benefits of this \enquote{bottom up} way of doing things, is that everysimplex is \enquote{context free} in the following sense:  For a subset $B \subset A$, the corresponding linear order simplicial complex for $B$ is a subcomplex of $A$, which conversely means it is easy to extend the set of alternatives.  With the \enquote{top down} approach, for extending $B$ to $A$, one would have to construct a new simplicial complex for $A$, in which one would embed $B$.  In this embedding, the a facet of $B$, which is a total order on $B$, would then not be implemented as total order on $B$ in the embedding, but as a set of all total orders on $A$ whose restriction to $B$ is that total order.  Any simplex is always to be viewed in context of the whole set of alternatives in this setting, even though it doesn't have to.

One can also now define a sort of \enquote{master} simplicial complex of linear orders, which would be one defined just as above, but with our set of alternatives being every single element that exists in our Grothendieck universe that we use for our Zermelo-Fraenkel model (more or less), so $A = \catname{Set}$.  This master simplicial complex then contains all linear order simplicial complex as a subcomplex (not just a canonically isomorphic subcomplex).

Usually, when looking at domain restrictions (for Arrow's impossibility), one looks at subcomplexes induced by a subset of the facets.  It might also be interesting for domain restrictions to look at simplices that do not represent linear orders, but pseudo linear orders (in particular weak linear orders, strict partial orders, etc.).  The question is, are there domain restrictions that permit a social welfare function, where there then exists no extension to a social welfare function defined on a complex that is induced by facets?  (This might be a very easy \emph{no} answer that I am not seeing right now.)

Let now $V$ be a finite set, the set of \emph{voters}.

\begin{definition}[profiled simplicial complex]
  Let $S$ be a chromatic simplicial complex.
  % Now consider, as our set of vertices of the simplicial complex we are constructing, the set $S^V$, meaning the set of all $V$-tuples of simplices in $S$, which is the set of all $\tuple{s_v}_{v \in V}$ where $s_v \in S$.
  For a subset $C \subseteq S^V$, we define for $v \in V$ the set $C_v \defeq \set{c_v \given c \in C}$, the \enquote{$v$-slice} of $C$.  We define the simplicial complex $S_V \defeq \set{C \subseteq S^V \given \forall v,w \in V \qcolon C_v \in S \commaand \chi(C_v) = \chi(C_w)}$.  This simplicial complex inherits the coloring of $S$.
\end{definition}

Our simplicial complex of voting profiles for alternatives $A$ and voters $V$ is then just $\mathfrak L_V$.  This is isomorphic to $N_P$ in \cite{bar93} via $\set{p} \mapsto U_{ab}^{\mathbf{\sigma}}$ where $\chi(\set{p}) = \set{\set{a,b}}$ and $\mathbf{\sigma}_v = \mathord -$ if $p_v = \tuple{a,b}$ and $\mathbf{\sigma}_v = \mathord +$ if $p_v = (b,a)$.

\begin{definition}[social welfare function]
  A cromatic map $\mathfrak L_V \to \mathfrak L$ is a social welfare function.
\end{definition}

\section{Muller-Satterthwaite and Arrow}

In \cite{br23}, the setup for the Muller-Satterthwaire theorem is to have as the domain of the social choice function the same simplicial complex as in the Arrow setting, and to have as the codomain the simplicial complex $N_A$ generated by the nerve of the collection of all $U_a = A \setminus \set a$ for $a \in A$.  It is shown that social choice functions that are unanimous and monotonic are in one-to-one correspondence with social choice functions $N_P \to N_A$ such that either $U_{ab}^{\mathbf{\sigma}} \mapsto U_a$ or $U_{ab}^{\mathbf{\sigma}} \mapsto U_b$, as well as $U_{ab}^{\mathbf{\sigma}} \mapsto U_a$ if $\mathbf{\sigma} = \mathord -^V$ and $U_{ab}^{\mathbf{\sigma}} \mapsto U_b$ if $\mathbf{\sigma} = \mathord +^V$.

Here I give an intuition behind what the $U_a$ represent:  They represent the set of all winners that are still possible, which is everything but $a$.  If we intersect two such sets $U_a$ and $U_b$, which in the nerve complex gives us a higher dimensional simplex, then we get the set $A \setminus \set{a,b}$, which means that everything but $a$ and $b$ can still win.  We intersect with more such sets, getting sets of the form $U_M = A \setminus M$ for $M \subsetneq A$, until only one element remains (meaning $U_M = U_{A \setminus \set{a}} = \set a$), which means that then $a$ is the winner.  This simplicial complex is just the boundary complex of the simplicial complex induced by the set $A$.

But how would one think about this in a \enquote{bottom up} way as before?  Which pseudo orders correspond to the simplices in this simplicial complex?  Every simplex in $N_A$ corresponds to a nonempty subset of $A$, namely $U_M$ corresponds to $A \setminus M \in \powerset{A}$.  As noted earlier, we can embed $\powerset{A} \setminus \set{\emptyset} \embarrow \Wea$ or $\powerset{A} \setminus \set{\emptyset} \embarrow \sWea$.  The embedding works by identifying $N \in \powerset{A} \setminus \set{\emptyset}$ with the weak order that ranks everything in $N$ on top with all of them in a tie, and ranking everything else below $N$, with those also being a tie.  So $U_M$ would correspond to ranking $A \setminus M$ on top, and ranking $M$ below that.

We sadly cannot do the same as before, starting with singletons and building up from there.  However, we can start with the following atomic elements (our $0$-simplices):  Each $U_a$ now corresponds to the set $T_a \defeq \set{\tuple{a,b} \in A \cartes A \given b \in A}$.  This is a (non strict!) pseudo weak order (in fact, it is also a partial order).  We can then take the union of multiple of these, where we then get $T_M = \set{ \tuple{a,b} \in A \cartes A \given a \in M \commaand b \in A}$.  Just as before, this means that every canditate that is not in $M$ can still win.  We do not allow $M = A$.  \footnote{We could technically take the union of all of these, where we would get $T_A = A \cartes A$, which paradoxically means that everyone is a winner, which is counter-intuitive because after taking the union with more and more such sets less and less candidates can still win, and then suddenly everyone is the winner instead of zero.}  Only when $M = \set a$ for some $a \in A$ does this become an actual weak order.  We call this simplicial complex $\mathfrak A$.

\begin{figure*}
  \centering
  \begin{tabular}{@{}c@{}}
    \begin{tikzpicture}[>={Stealth[round,sep]}]
      \graph [layered layout, nodes=draw, edges=rounded corners]
      {
        a1/$a_1$ -> { a3/$a_3$ };
        a2/$a_2$ -> { a3 };
        a4/$a_4$ -> { a3 };
        a5/$a_5$ -> { a3 };
        a6/$a_6$ -> { a3 };

        b1/$a_1$ -> { b5/$a_5$ };
        b2/$a_2$ -> { b5 };
        b3/$a_5$ -> { b5 };
        b4/$a_4$ -> { b5 };
        b6/$a_6$ -> { b5 };
      };
    \end{tikzpicture}  \\
    \begin{tikzcd}
      \phantom{a} \ar[d, "\text{union}"] \\ \phantom{a}
    \end{tikzcd}
    \\
    \begin{tikzpicture}
      \graph [layered layout, nodes=draw, edges=rounded corners]
      {
        c1/$a_1$ -> { c3/$a_3$, c5/$a_5$ };
        c2/$a_2$ -> { c3, c5 };
        c4/$a_4$ -> { c3, c5 };
        c6/$a_6$ -> { c5, c5};

        c3 <-> c5;

        { [same layer] c1, c2, c4, c6 };
        { [same layer] c3, c5 };
      };
    \end{tikzpicture}
  \end{tabular}
  \caption{The union of $T_{a_3}$ and $T_{a_5}$}
\end{figure*}

But we still not have achieved what we might want to achieve.  We again have the same issue as before:  The context of the set $A$ of all possible alternatives is always present in this setting.  Can we fix this?  Yes!

Remember that a $0$-simplex $s \in \mathfrak L_V$ such that $\chi(s) = \set{a,b}$ can be mapped to either $T_a$ or $T_b$.  But it somehow feels like $T_a$ and $T_b$ each don't reflect this dichotomy between $a$ and $b$:  The simplex $T_a \in \mathfrak A$ on its own has no connection to $b$ or any other particular element in $A$!  We can fix this.

\begin{definition}[collapsing map]
  We define the \emph{collapsing map} $\Gamma \mapcolon \mathfrak L_V \to \mathfrak A$ via $\set{\tuple{a,b}} \mapsto T_a$.
\end{definition}

\begin{remark}
  \begin{itemize}
  \item This is well-defined for hopefully obvious reasons.
  \item For $s \in \mathfrak A$, we have that $\Gamma(s) = T_M \in \mathfrak A$ where $M \subset A$ is the set of all elements in the pseudo linear order $s$ that are not maximal, that is, $A \setminus M$ is precisely the set of all maximal elements in $s$.
  \end{itemize}
\end{remark}

\begin{figure*}
  \centering
  \begin{tabular}{@{}c@{}}
    \begin{tikzpicture}
      \graph [layered layout, nodes=draw, edges=rounded corners]
      {
        { a1/$a_1$, a2/$a_2$ } -> {
          a3/$a_3$ } -> {
          a4/$a_4$ -> a11/$a_{11}$ -> a12/$a_{12}$, a5/$a_5$ -> a8/$a_8$ -> a11 , a9/$a_9$ };

        a7/$a_7$ -> a10/$a_{10}$ -> a11;
        a6/$a_6$ -> a9;
      };
    \end{tikzpicture}
    \\
      \begin{tikzcd}
        \phantom{a} \ar[d, "\text{collapse via $\Gamma$}", mapsto] \\ \phantom{a} 
      \end{tikzcd}
    \\
      \begin{tikzpicture}
        \graph [layered layout, nodes=draw, edges=rounded corners]
        {
          c1/$a_1$ -> { c3/$a_3$, c4/$a_4$, c5/$a_5$, c8/$a_8$, c9/$a_9$, c10/$a_{10}$, c11/$a_{11}$, c12/$a_{12}$ };
          c2/$a_2$ -> { c3/$a_3$, c4/$a_4$, c5/$a_5$, c8/$a_8$, c9/$a_9$, c10/$a_{10}$, c11/$a_{11}$, c12/$a_{12}$ };
          c6/$a_6$ -> { c3/$a_3$, c4/$a_4$, c5/$a_5$, c8/$a_8$, c9/$a_9$, c10/$a_{10}$, c11/$a_{11}$, c12/$a_{12}$ };
          c7/$a_7$ -> { c3/$a_3$, c4/$a_4$, c5/$a_5$, c8/$a_8$, c9/$a_9$, c10/$a_{10}$, c11/$a_{11}$, c12/$a_{12}$ };

          c3 <-> { c4, c5, c8, c9, c10, c11, c12 };
          c4 <-> { c5, c8, c9, c10, c11, c12 };
          c5 <-> { c8, c9, c10, c11, c12 };
          c8 <-> { c9, c10, c11, c12 };
          c9 <-> { c10, c11, c12 };
          c10 <-> { c11, c12 };
          c11 <-> { c12 };
          
          { [same layer] c3, c4, c5, c8, c9, c10, c11, c12 };
          { [same layer] c1, c2, c6, c7 };
        };
      \end{tikzpicture}
  \end{tabular}
  \caption{The collapsing map}
\end{figure*}

\begin{proposition}\label{ding}
  For any subcomplex $\mathfrak S \subseteq \mathfrak L_V$, a simplicial map $f \mapcolon \mathfrak S \to \mathfrak A$ such that $f(s) \in \set{T_a,T_b}$ for $\chi(s) = \set{\set{a,b}}$ uniquely factorizes through $\Gamma$, meaning there exists a unique simplicial map $\varphi \mapcolon \mathfrak L \to \mathfrak A$ such that $f = \Gamma \compos \varphi$.
  \begin{equation*}
    \begin{tikzcd}
      & \mathfrak L \ar[d, twoheadrightarrow, "\Gamma"] \\
      \mathfrak S \ar[ru, dashrightarrow, "\exunique \varphi"] \ar[r, "f"] & \mathfrak A
    \end{tikzcd}
  \end{equation*}
\end{proposition}

\begin{proof}
  TODO
\end{proof}

\begin{remark}
  $\varphi$ is a dictatorship if and only if $f$ is a dictatorship.
\end{remark}

\begin{corollary}
  There is a one-to-one correspondence between monotonous unanimous SCFs and pareto IIA SWFs for any domain restriction.
\end{corollary}

For the whole domain, this is not too surprising, because a posteriori, we know that only dictators are possible, and those can obviously be mapped to each other.  However, where things get interesting is when we look at domain restrictions.  Arrow and Muller-Satterthwaite become the same problem!

\begin{theorem}[Muller-Satterthwaite]
  [State the Muller-Satterthwaite theorem here]
\end{theorem}

\begin{proof}
  This is just a corollary of Arrow's impossibility theorem as in \cite{bar93} and \cite{rr22} together with \cref{ding}.  Use the proposition and $\mathfrak S = \mathfrak L_V$.  Then $\varphi$ will be a dictatorship, and then the remark gives us that we have a dictatorship.
\end{proof}

\newpage

\printbibliography

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
